import logo from './logo.svg';
import Footer from './footer/Footer'


import FirstHeader from './header/FirstHeader';
import NavBar from './navbar/NavBar';
import BodY from './body/BodY';

function App() {
  return (
    <div className="App">
      <FirstHeader />
      <NavBar/>
      <BodY/>
      <Footer/>
      
    </div>
  );
}

export default App;
