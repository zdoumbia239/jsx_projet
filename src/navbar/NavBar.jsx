
import logo from '../logo.svg';
import './Navbar.css';

export default function NavBar() {
  return (
    <div>
        <div className="container">
            <div className='firstContainer' >
                
                    <span > <img src={logo}></img></span>
                    <span >react</span>
            
                
            </div>
            <div className='secondContainer' >
                <ul>
                    <li><a href="">dogs</a></li>
                    <li><a href="">tutoriel</a></li>
                    <li><a href="">blog</a></li>
                    <li><a href="">communaute</a></li>
                
                </ul>
                
            </div>
            <div className='thirdContainer' >
            <ul>
                    <li><input type="text" /></li>
                    <li><a href="">v17.02</a></li>
                    <li> <span></span><a href="">langues</a></li>
                    <li> github</li>
                </ul>
                
                
            </div>
            
        </div>
    </div>
  )
}
